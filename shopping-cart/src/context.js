import React, {Component} from 'react';
import {storeProducts,detailProduct} from "./data";

const ProductContext = React.createContext()

class ProductProvider extends Component {
    state={
        products:[],
        detailProduct:detailProduct
    };
    componentDidMount() {
        this.setProducts();
    }

    setProducts = () =>{
        let tempProducts = [];
        storeProducts.forEach(item =>{
            const singleItem = {...item};
            tempProducts = [...tempProducts,singleItem]
        })
        this.setState(()=>{
            return {products:tempProducts}
        })
    }

    handleDetail = () =>{
        console.log('hello from details')
    }
    addToCart = () => {
        console.log('hello from addcart')
    };
    tester = () =>{
        console.log('state product :',this.state.products[0].inCart);
        console.log('data product :',storeProducts[0].inCart);

        const tempProducts = [...this.state.products];
        tempProducts[0].inCart = true;
        this.setState(()=>{
            return {product:tempProducts}
        },()=>{
            console.log('state product :',this.state.products[0].inCart);
            console.log('data product :',storeProducts[0].inCart);
        })

    }

    render() {

        return (
            <ProductContext.Provider value={{
                ...this.state,
                handleDetails:this.handleDetail,
                addToCart: this.addToCart
            }}>
                {/*<button onClick={this.tester}>Test me</button>*/}
                {this.props.children}
            </ProductContext.Provider>
        );
    }
}

const ProductConsumer = ProductContext.Consumer;


export {ProductProvider ,ProductConsumer};
//export default ProductProvider;