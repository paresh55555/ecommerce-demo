import React, {Component} from 'react';
import Register from "./Register";
import {Link} from "react-router-dom";

class Login extends Component {
    constructor(props) {
        super(props);
        this.state = {
            username: "",
        };
    }
    handleChange =(e)=>{
        const {name, value} = e.target;

        this.setState({
            [name] : value,

        });
    };
    addWithValidation = (nameuser)=> {
        if (this.state.username === nameuser) {
           alert('data check...')

        }
    };
    render() {
        const {id,items,username,email,number}= this.state;
        return (
            <div>
                <div className="col-lg-6 login">
                    <label className="center"><h1 >Login Page</h1></label>
                    <hr/>
                    <div className="col-lg-6">
                        <label for="user name">User Name</label>
                        <input type="text" name='username' onChange={(e)=>{this.handleChange(e)}} value={username}  className="form-control form-width"/>
                    </div>
                    <div className="col-lg-6">
                        <label htmlFor="password">Password</label>
                        <input type="password" className="form-control form-width"/>
                    </div>
                    <br/>
                    <div className="col-lg-3">
                        <button className="btn btn-primary" onClick={this.addWithValidation('paresh')}>
                            Login</button>
                    </div>
                    <hr/>
                    <div className="center">
                        <Link to="/register">
                            <p>Register</p>
                        </Link>
                    </div>
                </div>

            </div>
        );
    }
}

export default Login;