import React, {Component} from 'react';
import {Link} from "react-router-dom";

class Register extends Component {
    render() {
        return (
            <div>
                <div className="col-lg-6 register">
                    <label className="center"><h1>Register  </h1></label>
                    <hr/>
                    <div className="col-lg-6">
                        <label htmlFor="user name">First Name</label>
                        <input type="text" className="form-control form-width"/>
                    </div>
                    <div className="col-lg-6">
                        <label htmlFor="last name">Last Name</label>
                        <input type="text" className="form-control form-width"/>
                    </div>
                    <div className="col-lg-6">
                        <label htmlFor="email id">Email ID</label>
                        <input type="email" className="form-control form-width"/>
                    </div>
                    <div className="col-lg-6">
                        <label htmlFor="contact">Contact No</label>
                        <input type="text" className="form-control form-width"/>
                    </div>
                    <br/>
                    <div className="col-lg-3">
                        <button className="btn btn-primary">Submit</button>
                    </div>
                    <hr/>
                    <div className="center">
                        <Link to="/">
                            <a href="">Already have account ?</a>
                        </Link>
                    </div>
                </div>

            </div>
        );
    }
}

export default Register;